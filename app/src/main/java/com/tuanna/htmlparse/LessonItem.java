package com.tuanna.htmlparse;

/**
 * Copyright @2017
 * Created by TuanNA on 30/06.
 */

public class LessonItem {
    private String title;
    private String thumbnail;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
