package com.tuanna.htmlparse;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Copyright @2017
 * Created by TuanNA on 30/06.
 */

public final class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHolder> {
    private final List<LessonItem> mLessons;
    private Context mContext;
    private OnLessonListener mListener;

    public LessonAdapter(Context context, List<LessonItem> lessons, OnLessonListener listener) {
        this.mContext = context;
        this.mLessons = lessons;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_lesson_cell, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        LessonItem item = mLessons.get(holder.getAdapterPosition());
        holder.tvTitle.setText(TextUtils.isEmpty(item.getTitle()) ? "" : item.getTitle());
        if (!TextUtils.isEmpty(item.getThumbnail())) {
            Picasso.with(mContext)
                    .load(item.getThumbnail())
                    .error(R.mipmap.ic_launcher_round)
                    .into(holder.thumbnail);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onLessonClick(holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLessons.size();
    }

    public interface OnLessonListener {
        void onLessonClick(int adapterPosition);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        ImageView thumbnail;

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            thumbnail = (ImageView) itemView.findViewById(R.id.imgThumbnail);
        }
    }
}
