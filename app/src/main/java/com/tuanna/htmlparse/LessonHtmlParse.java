package com.tuanna.htmlparse;

import android.util.Log;

import com.tuanna.htmlparse.http.Response;
import com.tuanna.htmlparse.in.HtmlParse;
import com.tuanna.htmlparse.model.HtmlElement;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Copyright @2017
 * Created by TuanNA on 30/06.
 */

public class LessonHtmlParse implements HtmlParse {
    @Override
    public HtmlElement parseHtml(String htmlToParse) {
        Response<LessonItem> response = new Response<>();
        try {
            Document doc = Jsoup.parse(htmlToParse);
            response.setElements(new ArrayList<LessonItem>());
            Elements elements = doc.select("div.gallery-item");
            for (Element element : elements) {
                LessonItem item = new LessonItem();
                item.setTitle(element.text());
                item.setThumbnail((element.getElementsByTag("img").first()).absUrl("src"));
                response.getElements().add(item);
            }
        } catch (Exception exception) {
            Log.d(LessonHtmlParse.class.getName(), exception.getMessage());
        }
        return response;
    }
}
