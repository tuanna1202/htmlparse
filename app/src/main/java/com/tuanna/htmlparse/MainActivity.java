package com.tuanna.htmlparse;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tuanna.htmlparse.http.Callback;
import com.tuanna.htmlparse.http.HttpRequest;
import com.tuanna.htmlparse.http.HttpResponse;
import com.tuanna.htmlparse.http.Response;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LessonAdapter.OnLessonListener {
    LessonAdapter mAdapter;
    List<LessonItem> mLessonItems;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init views
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        final RecyclerView rvLesson = (RecyclerView) findViewById(R.id.rlLesson);

        // get and set value
        mLessonItems = new ArrayList<>();
        rvLesson.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.tvGetLesson).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressBar.setVisibility(View.VISIBLE);
                        try {
                            String url = "http://600tuvungtoeic.com/";
                            final HttpRequest request = new HttpRequest(url);
                            request.setHtmlParser(new LessonHtmlParse());
                            request.execute(BaseApplication.getInstance().getRequestQueue(), new Callback() {

                                @Override
                                public void onSuccess(HttpResponse response) {
                                    progressBar.setVisibility(View.GONE);
                                    if (response == null) {
                                        return;
                                    }
                                    Response<LessonItem> typedObject = (Response<LessonItem>) response.getTypedObject();
                                    mLessonItems = typedObject.getElements();
                                    mAdapter = new LessonAdapter(MainActivity.this, mLessonItems, MainActivity.this);
                                    rvLesson.setAdapter(mAdapter);
                                }

                                @Override
                                public void onError(String errorData) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(MainActivity.this, errorData, Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (Exception e) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, "Some error occurred." + e.getMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }
        );
    }

    @Override
    public void onLessonClick(int adapterPosition) {
        Toast.makeText(MainActivity.this, mLessonItems.get(adapterPosition).getTitle(), Toast.LENGTH_SHORT).show();
    }
}
